import Moment from "moment";
import {Calendar} from "primereact/calendar";
import {Button} from "primereact/button";
import {DataTable} from "primereact/datatable";
import {Column} from "primereact/column";
import {SERVER_URL} from '../src/constants.js'
import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import React from "react";

class CurrencyChanges extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            changes: [],
            date: new Date('2014.01.01'),
            error: '',
            minDate: new Date('1994.01.02'),
            maxDate: new Date('2014.12.31'),
            warning: '',
            lastFetchedDate: ''
        };
    }

    fetchChanges = () => {
        if (this.state.date === this.state.lastFetchedDate) {
            this.setState({warning: 'no new records to fetch'});
            return;
        }
        this.setState({warning: ''});

        const date = Moment(this.state.date).format('YYYY.MM.DD')

        const url = `${SERVER_URL}/api/currency/changes?date=${date}`;
        fetch(url, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(response => response.json())
            .then(responseData => {
                this.setState({
                    changes: responseData.changes,
                    error: responseData.error.message,
                    lastFetchedDate: responseData.error.message === '' ? this.state.date : ''
                });
            }).catch(err => {
            console.error(err);
            this.setState({
                error: 'server error, for more details check console'
            });
        });
    }

    render() {
        const error = this.state.error === '' ? '' : <p className="error-box">{this.state.error}</p>
        const warning = this.state.warning === '' ? '' : <p className="warning-box">{this.state.warning}</p>
        return (
            <div>
                <h3>Please select date and click button below in order to receive currency changes</h3>
                <Calendar minDate={this.state.minDate} maxDate={this.state.maxDate} value={this.state.date}
                          onChange={(e) => this.setState({date: e.value})}
                          dateFormat="yy.mm.dd" monthNavigator={true} yearNavigator={true}
                          yearRange="1994:2014">

                </Calendar>
                <br/>
                <Button label="Fetch changes" onClick={this.fetchChanges}/>
                {error}
                {warning}
                <br/>
                <DataTable value={this.state.changes}>
                    <Column field="dateRange" header="Date range"/>
                    <Column field="oldRatio" header="Old rate"/>
                    <Column field="newRatio" header="New rate"/>
                    <Column field="currency" header="Currency"/>
                    <Column field="ratioDiff" header="Rate increase"/>
                </DataTable>
            </div>
        );
    }
}

export default CurrencyChanges;