import React, {Component} from 'react';


import './App.css';
import CurrencyChanges from "./CurrencyChanges";

class App extends Component {

    render() {
        return (
            <div className="App">
                <CurrencyChanges/>
            </div>
        );

    }
}

export default App;
